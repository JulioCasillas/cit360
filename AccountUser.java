/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.*;

/**
 *
 * @author julio
 */
public class AccountUser {
    
    public static void main(String[] args){
        //TODO Auto-generated method stub
         
        Client cs1=new Client("Mayra Lopez", "0001", 123456);
        Client cs2=new Client("John Smith", "0002", 234567);
        Client cs3=new Client("Brad Pitt", "0003", 345678);
        Client cs4=new Client("Antonio Banderas", "0004", 456789);
        Client cs5=new Client("Sara Noble", "0006", 567890);
        Client cs6=new Client("Bradley Noble", "0007", 678901);
        
        Set <Client> BankClients=new HashSet<Client>();
        
        BankClients.add(cs1);
        BankClients.add(cs2);
        BankClients.add(cs3);
        BankClients.add(cs4);
        BankClients.add(cs5);
        BankClients.add(cs6);
        
        System.out.println("Costumer" + " | " + "Client Number" + " | " + "Money");
        
        for (Client client : BankClients){
            System.out.println(client.getName() + " | " + client.getAccount() + " | "
            + client.getMoney());
        }
        
    }
    
}
