/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author julio
 */
public class Client {
    
    public Client(String name, String account, double money){
        this.name=name;
        this.account=account;
        this.money=money;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public String getName() {
        return name;
    }

    public String getAccount() {
        return account;
    }

    public double getMoney() {
        return money;
    }

    
    private String name;
    private String account;
    private double money;
    
}
