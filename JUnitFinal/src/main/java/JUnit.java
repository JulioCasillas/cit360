public class JUnit {

    static long result;

    public static long add(long a, long b) {
        try {
            result = a + b;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    public static long subtract(long a, long b) {
        try {
            result = a - b;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    public static long multiply(long a, long b) {
        try {
            result = a * b;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    public static long divide(long a, long b) {
        try {
            result = a / b;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}
