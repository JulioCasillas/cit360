import org.junit.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class  JUnitTest {

    @Test
    public void add(){
        final long result = new JUnit().add(5, 5);
        assertThat(result, is(10L));
    }

    @Test
    public void subtract() throws Exception {
        final long result = new JUnit().subtract(10, 5);
        assertThat(result, is(5L));
    }

    @Test
    public void multiply() throws Exception {
        final long result = new JUnit().multiply(2, 3);
        assertThat(result, is(6L));
    }
    @Test
    public void divide() throws Exception {
        final long result = new JUnit().divide(3, 3);
        assertThat(result, is(1L));
    }

}