/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.*;

/**
 *
 * @author julio
 */
public class Lista {
    
     public static void main(String[] args) {
         ArrayList<Integer> array = readValues();
         double sum = calculateSum(array);
         double media = sum / array.size();
         showResults(array, sum, media);
   }

   public static ArrayList<Integer> readValues() {
        ArrayList<Integer> valores = new ArrayList();
        Scanner sc = new Scanner(System.in);
        int n;
        System.out.print("Introduce a number. 0 to finish: ");
        n = sc.nextInt();
        while (n != 0) {
                 valores.add(n);
                 System.out.print("Introduce a number. 0 to finish: ");
                 n = sc.nextInt();
        }
        return valores;
   }

   public static double calculateSum(ArrayList<Integer> values) {
         double sum = 0;
         Iterator it = values.iterator();
         while (it.hasNext()) {
                  sum = sum + (Integer) it.next();
         }
         return sum;
   }

   public static void showResults(ArrayList<Integer> values, double sum, double media) {
         int cont = 0;
         System.out.println("Input Values: ");
         System.out.println(values);
         System.out.println("Sum: " + sum);
         System.out.printf("Media: %.2f %n", media);
         for (Integer i : values) {
               if (i > media) {
                  cont++;
              }
         }
      System.out.println(cont + " values over the media");
   }
    
}
