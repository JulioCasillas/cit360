package hibernateMenu;



import antlr.collections.AST;

import javax.persistence.*;

/** This Data Object class corresponds with customer table
 *  in database. */
@Entity
@Table(name = "food")
public class Menu {

    public int getIdFood() {
        return idFood;
    }

    public void setIdFood(int idFood) {
        this.idFood = idFood;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /** id is an identity type field in the database and the primary key */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idFood")
    private int idFood;

    @Column(name = "plate")
    private String plate;

    @Column(name = "price")
    private String price;

    @Column(name = "description")
    private String description;




    public String toString() {
        return Integer.toString(idFood) + " " + plate + " " + price + " " + description;
    }

    public void getJSONFood() {
    }

}
