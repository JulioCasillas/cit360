package hibernateMenu;

import
org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;
public class TestDAO {

    static SessionFactory factory = null;
    static Session session = null;

    private static TestDAO single_instance = null;

    private TestDAO()
    {
        factory = HibernateUtils.getSessionFactory();
    }


    public static TestDAO getInstance()
    {
        if (single_instance == null) {
            single_instance = new TestDAO();
        }

        return single_instance;
    }


    public static List<Menu> getFood() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from hibernateMenu.Menu";
            List<Menu> cs = (List<Menu>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    public Menu getFood(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from hibernateMenu.Menu where id=" + Integer.toString(id);
            Menu c = (Menu)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return c;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}