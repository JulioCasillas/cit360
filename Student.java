/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.*;

/**
 *
 * @author julio
 */
public class Student {
    public static void main(String args[])
    {
		HashMap global = new HashMap();
 
		global.put("Joseph", "123456789");
		global.put("Charlie", "234567890");
		global.put("Peter", "345678901");
		global.put("Daniel", "456789012");
		global.put("Artur", "567890123");
                
                System.out.println("Student: " + " | " + "Student ID: ");
 
		for( Iterator it = global.keySet().iterator(); it.hasNext();) { 
			String student = (String)it.next();
			String id = (String)global.get(student);
			System.out.println(student + " | " + id);
		}
	}
}

