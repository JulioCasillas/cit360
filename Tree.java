/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.*; 

/**
 *
 * @author julio
 */
public class Tree {
    
        static void ExampleTree() 
    { 

        TreeMap<String, String> global = new TreeMap<>();

        global.put("Joseph", "123456789");
        global.put("Charlie", "234567890");
        global.put("Peter", "345678901");
        global.put("Daniel", "456789012");
        global.put("Artur", "567890123");

        System.out.println("TreeMap: " + global); 
    } 
  
    public static void main(String[] args) 
    { 
  
        System.out.println("TreeMap using " + "TreeMap() constructor:\n"); 
        ExampleTree(); 
    } 
    
}
