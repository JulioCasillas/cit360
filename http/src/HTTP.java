import org.jetbrains.annotations.Contract;

import java.net.*;
import java.util.*;

public class HTTP {

    private static String Content;

    public static void main(String[] args) {

        try {
            URL url = new URL("https://www.wdl.org/es/");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            Map headers = conn.getHeaderFields();
            Set<String> keys = headers.keySet();
            System.out.println("Content can be found in:" + url.getContent() );
            System.out.println("The file name is: " + url.getFile());
            System.out.println("The user info is: " + url.getUserInfo());
            System.out.println("The Host is:" + url.getHost());
            System.out.println("The content is: " + url.getContent());
            for (String key : keys) {
                String value = conn.getHeaderField(key);
                System.out.println(key + " -> " + value);

            }
            System.out.println(conn.getLastModified());

        } catch (Exception e) {
            System.err.println(e.toString());
        }



    }
    

}